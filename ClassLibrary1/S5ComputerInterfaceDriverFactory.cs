﻿using System;
using System.Collections.Generic;
using Driver.Abstractions;
using Driver.Base;
using Extensions.Logging;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Driver.S5ComputerInterface
{
    public class S5ComputerInterfaceDriverFactory : BaseDriverFactory<S5ComputerInterfaceDriverSettings>
    {
        public S5ComputerInterfaceDriverFactory(ILoggerFactory loggerFactory) : base(loggerFactory)
        {
        }

        public override IDriver Create(string uniqueDeviceId, JObject driverSettings)
        {
            var settings = ParseSettings(driverSettings);

            var loggingContext = new Dictionary<string, object>()
            {
                {"DeviceId",  uniqueDeviceId}
            };
            
            var transport = TransportLayerFactory.Create(settings.Transport, loggingContext);

            
            var sessionLayerLogger = new LoggerWithContext<S5ComputerInterfaceSessionProtocolLayer>(LoggerFactory.CreateLogger<S5ComputerInterfaceSessionProtocolLayer>(), loggingContext);
            var session = new S5ComputerInterfaceSessionProtocolLayer(transport, sessionLayerLogger);

            var driverLogger =
                new LoggerWithContext<S5ComputerInterfaceDriver>(
                    LoggerFactory.CreateLogger<S5ComputerInterfaceDriver>(), loggingContext);

            return new S5ComputerInterfaceDriver(uniqueDeviceId, 5, session, driverLogger);
        }
    }
}