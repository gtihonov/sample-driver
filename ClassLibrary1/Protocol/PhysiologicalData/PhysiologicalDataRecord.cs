using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PhysiologicalDataRecord : IEnumerable<PhysiologicalDataSubrecord>
    {
        public PhysiologicalDataSubrecord Subrecord0;
        public PhysiologicalDataSubrecord Subrecord1;
        public PhysiologicalDataSubrecord Subrecord2;
        public PhysiologicalDataSubrecord Subrecord3;
        public PhysiologicalDataSubrecord Subrecord4;

        public IEnumerator<PhysiologicalDataSubrecord> GetEnumerator()
        {
            yield return Subrecord0;

            yield return Subrecord1;

            yield return Subrecord2;

            yield return Subrecord3;

            yield return Subrecord4;

            yield break;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}