using System.Runtime.InteropServices;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class AnesthesiaAgent2Group
    {
        public GroupHeader Header { get; set; }

        public short MacAgeSub { get; set; }

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] Reserved;
    }
}