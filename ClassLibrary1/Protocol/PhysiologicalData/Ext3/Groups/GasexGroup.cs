using System.Runtime.InteropServices;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class GasexGroup
    {
        public GroupHeader Header { get; set; }

        public short Vo2 { get; set; }
        public short VCo2 { get; set; }
        public short Ee { get; set; }
        public short Rq { get; set; }
    }
}