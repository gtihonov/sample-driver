using System.Runtime.InteropServices;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class TonometryGroup
    {
        public GroupHeader Header { get; set; }

        public short Prco2 { get; set; }

        public short PrEt { get; set; }

        public short PrPa { get; set; }

        public short PaDelay { get; set; }

        public short Phi { get; set; }

        public short PhiDelay { get; set; }

        public short AmbPress { get; set; }

        public short Cpma { get; set; }
    }
}