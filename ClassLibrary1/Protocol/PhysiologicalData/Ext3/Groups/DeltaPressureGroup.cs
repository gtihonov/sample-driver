using System.Runtime.InteropServices;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class DeltaPressureGroup
    {
        public GroupHeader Header { get; set; }

        public short Spv { get; set; }

        public short Ppv { get; set; }
    }
}