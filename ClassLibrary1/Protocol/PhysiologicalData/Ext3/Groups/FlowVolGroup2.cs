using System.Runtime.InteropServices;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3.Groups
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class FlowVolGroup2
    {
        public GroupHeader Header { get; set; }

        public short Ipeep { get; set; }

        public short Pmean { get; set; }

        public short Raw { get; set; }

        public short MvInsp { get; set; }

        public short Epeep { get; set; }

        public short MvSpont { get; set; }

        public short IeRatio { get; set; }

        public short InspTime { get; set; }

        public short ExpTime { get; set; }

        public short StaticCompliance { get; set; }

        public short StaticPplat { get; set; }

        public short StaticPeepe { get; set; }

        public short StaticPeepi { get; set; }

        public short Reserved0 { get; set; }

        public short Reserved1 { get; set; }

        public short Reserved2 { get; set; }

        public short Reserved3 { get; set; }

        public short Reserved4 { get; set; }

        public short Reserved5 { get; set; }

        public short Reserved6 { get; set; }

        public short Reserved7 { get; set; }
    }
}