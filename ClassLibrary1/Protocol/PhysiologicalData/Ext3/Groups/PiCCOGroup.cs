using System.Runtime.InteropServices;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PiCCOGroup
    {
       public GroupHeader Header { get; set; }

       public short Cci { get; set; }

       public short Cco { get; set; }

       public short Cfi { get; set; }

       public short Ci { get; set; }

       public short Co { get; set; }

       public short Cpi { get; set; }

       public short Cpo { get; set; }

       public short Dpmax { get; set; }

       public short Elwi { get; set; }

       public short Evlw { get; set; }

       public short Gedi { get; set; }

       public short Gedv { get; set; }

       public short Gef { get; set; }

       public short Itbi { get; set; }

       public short Itbv { get; set; }

       public short Ppv { get; set; }

       public short Pvpi { get; set; }

       public short Sv { get; set; }

       public short Svi { get; set; }

       public short Svr { get; set; }

       public short Svri { get; set; }

       public short Svv { get; set; }

       public short Tblood { get; set; }

       public short Tinj { get; set; }
    }
}