using System.Runtime.InteropServices;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3.Groups;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PsychologicalExt3Subrecord
    {
        public GasexGroup Gasex { get; set; }

        public FlowVolGroup2 FlowVol2 { get; set; }

        public BalanceGasGroup Bal { get; set; }

        public TonometryGroup Tono { get; set; }

        public AnesthesiaAgent2Group Aa2 { get; set; }

        public DeltaPressureGroup Delp { get; set; }

        public CppGroup Cpp { get; set; }

        public CppGroup Cpp2 { get; set; }

        public PiCCOGroup Picco { get; set; }
    }
}