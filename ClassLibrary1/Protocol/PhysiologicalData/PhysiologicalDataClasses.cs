﻿using System;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{
    [Flags]
    public enum PhysiologicalDataClasses
    {
        DRI_PHDBCL_REQ_BASIC_MASK = 0x0000,
        DRI_PHDBCL_DENY_BASIC_MASK = 0x0001,
        DRI_PHDBCL_REQ_EXT1_MASK = 0x0002,
        DRI_PHDBCL_REQ_EXT2_MASK = 0x0004,
        DRI_PHDBCL_REQ_EXT3_MASK = 0x0008,
    }
}