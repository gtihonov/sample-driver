﻿namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{
    public enum PhysiologicalDataClass : byte
    {
        DRI_PHDBCL_BASIC,
        DRI_PHDBCL_EXT1,
        DRI_PHDBCL_EXT2,
        DRI_PHDBCL_EXT3,
    }
}
