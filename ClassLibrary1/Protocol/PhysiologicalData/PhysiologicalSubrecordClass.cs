﻿namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{
    public enum PhysiologicalSubrecordClass
    {
        Basic,
        Ext1,
        Ext2,
        Ext3
    }
}