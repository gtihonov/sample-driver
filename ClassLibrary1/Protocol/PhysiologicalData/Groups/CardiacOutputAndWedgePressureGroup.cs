﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CardiacOutputAndWedgePressureGroup
    {
        public GroupHeader Header { get; set; }

        public short Co { get; set; }

        public short BloodTemp { get; set; }

        public short Ref { get; set; }

        public short Pcwp { get; set; }
    }
}
