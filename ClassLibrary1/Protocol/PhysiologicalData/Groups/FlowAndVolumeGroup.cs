﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct FlowAndVolumeGroup
    {
        public GroupHeader Header { get; set; }

        public short Rr { get; set; }

        public short Ppeak { get; set; }

        public short Peep { get; set; }

        public short Pplat { get; set; }

        public short TvInsp { get; set; }

        public short TvExp { get; set; }

        public short Compiance { get; set; }

        public short MvExp { get; set; }
    }
}
