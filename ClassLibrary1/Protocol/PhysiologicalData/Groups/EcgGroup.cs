﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct EcgGroup
    {
        public GroupHeader Header { get; set; }

        public short HeartRate { get; set; }

        public short ST1 { get; set; }

        public short ST2 { get; set; }

        public short ST3 { get; set; }

        public short RespirationRate { get; set; }
    }
}
