﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct EcgExtraGroup
    {
        public short Hr_Ecg { get; set; }

        public short Hr_Max { get; set; }

        public short Hr_Min { get; set; }
    }
}
