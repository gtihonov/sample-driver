﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct O2Group
    {
        public GroupHeader Header { get; set; }

        public short Et { get; set; }

        public short Fi { get; set; }
    }
}
