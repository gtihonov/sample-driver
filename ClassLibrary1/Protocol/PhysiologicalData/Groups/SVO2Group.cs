﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SVO2Group
    {
        public GroupHeader Header { get; set; }

        public short SVo2 { get; set; }
    }
}
