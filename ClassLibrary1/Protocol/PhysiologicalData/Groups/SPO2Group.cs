﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SPO2Group
    {
        public GroupHeader Header { get; set; }

        public short SpO2 { get; set; }

        public short PulseRate { get; set; }

        public short Modulation { get; set; }

        public short SvO2 { get; set; }
    }
}
