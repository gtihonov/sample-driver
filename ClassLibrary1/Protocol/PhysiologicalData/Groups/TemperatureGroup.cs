﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct TemperatureGroup
    {
        public GroupHeader Header { get; set; }

        public short Temp { get; set; }
    }
}
