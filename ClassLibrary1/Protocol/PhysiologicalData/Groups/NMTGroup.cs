﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct NMTGroup
    {
        public GroupHeader Header { get; set; }

        public short T1 { get; set; }

        public short Tratio { get; set; }

        public short Ptc { get; set; }
    }
}
