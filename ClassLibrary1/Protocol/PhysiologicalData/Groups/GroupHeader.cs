﻿﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{

    // TODO: Реализовать струткру статуса

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GroupHeader
    {
        public int Status { get; set; }
        
        public short Label { get; set; }
    }
}
