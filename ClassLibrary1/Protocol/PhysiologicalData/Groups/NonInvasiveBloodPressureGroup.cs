﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct NonInvasiveBloodPressureGroup
    {
        public GroupHeader Header { get; set; }

        public short Sys { get; set; }

        public short Dia { get; set; }

        public short Mean { get; set; }

        public short PulseRate { get; set; }
    }
}
