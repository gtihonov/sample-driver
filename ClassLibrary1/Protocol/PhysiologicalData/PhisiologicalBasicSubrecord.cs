﻿using System.Runtime.InteropServices;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Groups;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PhisiologicalBasicSubrecord
    {
        public EcgGroup Ecg { get; set; }

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14 * 4)]
        public byte[] InvasivePressure1234;

        public NonInvasiveBloodPressureGroup NonInvasiveBloodPressure;

        public TemperatureGroup Temperature0;

        public TemperatureGroup Temperature1;

        public TemperatureGroup Temperature2;

        public TemperatureGroup Temperature3;

        public SPO2Group Spo2 { get; set; }


        public CO2Group Co2 { get; set; }

        public O2Group O2 { get; set; }

        public N2OGroup N2o { get; set; }

        public AnesthesiaAgentGroup Aa { get; set; }

        public FlowAndVolumeGroup FlowAndVolume { get; set; }

        public CardiacOutputAndWedgePressureGroup CoWedge { get; set; }

        public NMTGroup Nmt { get; set; }

        public EcgExtraGroup EcgExtra { get; set; }

        public SVO2Group Svo2 { get; set; }
        
    }
}
