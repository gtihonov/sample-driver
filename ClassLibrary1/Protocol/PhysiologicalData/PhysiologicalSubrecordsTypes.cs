﻿namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{
    public enum PhysiologicalSubrecordsTypes : byte
    {
        DRI_PH_DISPL = 1,
        DRI_PH_10S_TREND = 2,
        DRI_PH_60S_TREND = 3,
        DRI_PH_AUX_INFO = 4
    }


}
