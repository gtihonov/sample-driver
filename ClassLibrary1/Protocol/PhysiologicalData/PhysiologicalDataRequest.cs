﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PhysiologicalDataRequest
    {
        public PhysiologicalSubrecordsTypes RecordType { get; }

        public short Interval { get; }

        public PhysiologicalDataClasses SubrecordType { get; }

        public short Reserved { get; }

        public PhysiologicalDataRequest(PhysiologicalSubrecordsTypes recordType, short interval,
            PhysiologicalDataClasses subrecordType)
        {
            RecordType = recordType;
            Interval = interval;
            SubrecordType = subrecordType;
        }

    }
}
