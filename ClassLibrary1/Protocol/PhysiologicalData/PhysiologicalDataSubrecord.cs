using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{
    [StructLayout(LayoutKind.Sequential, Size = 278, Pack = 1)]
    public class PhysiologicalDataSubrecord
    {
        public uint Time;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 270)]
        public byte[] Payload;

        public byte Marker;

        public byte Reserved;

        public ushort ClDrilvlSubt;
        
    }
}