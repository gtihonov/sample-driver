using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.PhysiologicalData
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PhysiologicalDataRequestRecord : BaseRecord
    {
        public PhysiologicalDataRequest PhysiologicalDataRequest;
    }
}