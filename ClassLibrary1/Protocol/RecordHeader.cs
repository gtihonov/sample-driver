﻿using System;
using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class RecordHeader
    {
        public short Length;

        public byte RecordNumber;

        public byte DriLevel;

        public ushort PlugId;

        public uint RelativeTime;

        public byte Subnet;

        public byte Reserved1;

        public ushort Reserved2;

        public RecordMainType RecordMainType;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public SubrecordDescription[] SubrecordDescriptions = new SubrecordDescription[8];
    }
}
