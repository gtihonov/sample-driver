﻿namespace Driver.S5ComputerInterface.Protocol
{
    public enum RecordMainType : short
    {
        DRI_MT_PHDB = 0,
        DRI_MT_WAVE = 1,
        DRI_MT_ALARM = 4,
        DRI_MT_NETWORK = 5,
        DRI_MT_FO = 8
    }

    
}