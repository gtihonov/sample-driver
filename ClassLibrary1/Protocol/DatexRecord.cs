﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class DatexRecord : BaseRecord
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1450)]
        public byte[] Payload;

    }

}
