namespace Driver.S5ComputerInterface.Protocol
{
    public enum FramingFlags
    {
        FrameChar = 0x7E,
        CtrlChar = 0x7D
    }
}