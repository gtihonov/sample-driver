﻿using System.Collections.Generic;

namespace Driver.S5ComputerInterface.Protocol
{
    public static class ChecksumUtils
    {
        public static byte Calculate(IEnumerable<byte> bytes)
        {
            byte result = 0;

            foreach (var b in bytes)
            {
                result += b;
            }

            return result;
        }
    }
}
