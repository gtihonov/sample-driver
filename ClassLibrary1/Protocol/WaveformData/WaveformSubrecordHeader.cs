using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.WaveformData
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class WaveformSubrecordHeader
    {
        public short ActualLength { get; set; }
        
        public ushort Status { get; set; }
        
        public ushort Reserved { get; set; }
    }
}