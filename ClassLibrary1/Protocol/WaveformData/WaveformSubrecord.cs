namespace Driver.S5ComputerInterface.Protocol.WaveformData
{
    public class WaveformSubrecord
    {
        public WaveformSubrecordHeader Header { get; set; }
        
        public short[] Samples { get; set; }
    }
}