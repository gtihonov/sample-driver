using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.WaveformData
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class WaveformRequest
    {
        public WaveformRequestType RequestType { get; }
        
        public short Res { get; set; }
        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public WaveformType[] WaveformTypes = new WaveformType[8];

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public short[] Reserved = new short[10];

        public WaveformRequest(WaveformRequestType requestType)
        {
            RequestType = requestType;
        }
    }
}