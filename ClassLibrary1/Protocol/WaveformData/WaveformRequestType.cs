namespace Driver.S5ComputerInterface.Protocol.WaveformData
{
    public enum WaveformRequestType : short
    {
        WF_REQ_CONT_START,
        WF_REQ_CONT_STOP
    }
}