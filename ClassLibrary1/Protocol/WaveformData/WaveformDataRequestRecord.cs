using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol.WaveformData
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class WaveformDataRequestRecord : BaseRecord
    {
        public WaveformRequest WaveformRequest { get; set; }
    }
}