using System;
using System.Collections;
using System.Collections.Generic;
using Extensions.Utils;

namespace Driver.S5ComputerInterface.Protocol.WaveformData
{
    public class WaveformDataSubrecords : IEnumerable<(WaveformType Type, WaveformSubrecord WaveformSubrecord)>
    {
        private readonly SubrecordDescription[] _subrecordDescriptions;
        private readonly Memory<byte> _payload;

        public WaveformDataSubrecords(SubrecordDescription[] subrecordDescriptions, Memory<byte> payload)
        {
            _subrecordDescriptions = subrecordDescriptions;
            _payload = payload;
        }

        public IEnumerator<(WaveformType Type, WaveformSubrecord WaveformSubrecord)> GetEnumerator()
        {
            foreach (var subrecordDescription in _subrecordDescriptions)
            {
                if (subrecordDescription.SubrecordType == (byte)WaveformType.DRI_EOL_SUBR_LIST)
                {
                    break;
                }

                var subrecordPayload = _payload.Slice(subrecordDescription.SubrecordOffset);

                var headerBytes = subrecordPayload.Slice(0, 6);

                var header = SerializationUtils.BytesToStruct<WaveformSubrecordHeader>(headerBytes.ToArray());
                var data = _payload.Slice(subrecordDescription.SubrecordOffset + 6, header.ActualLength).ToArray();
                
                var samples = new List<short>(data.Length / 2);

                for (int n = 0; n < data.Length; n += 2)
                {

                    if (n == data.Length - 1)
                    {
                        break;
                    }

                    samples.Add(BitConverter.ToInt16(data, n));
                }

                yield return ((WaveformType)subrecordDescription.SubrecordType, new WaveformSubrecord()
                {
                    Header = header,
                    Samples = samples.ToArray()
                });
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}