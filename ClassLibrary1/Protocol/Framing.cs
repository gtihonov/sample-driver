﻿using System.Collections.Generic;
using System.Linq;

namespace Driver.S5ComputerInterface.Protocol
{
    public static class Framing
    {
        public static byte[] EscapeFlagBytes(IEnumerable<byte> bytes)
        {
            var fixedPayload = new List<byte>(bytes.Count());

            foreach (var payloadByte in bytes)
            {
                if (payloadByte == (byte)FramingFlags.FrameChar)
                {
                    fixedPayload.Add((byte)FramingFlags.CtrlChar);
                    fixedPayload.Add(0x5E);
                } else if (payloadByte == (byte)FramingFlags.CtrlChar)
                {
                    fixedPayload.Add((byte)FramingFlags.CtrlChar);
                    fixedPayload.Add(0x5D);
                }
                else
                {
                    fixedPayload.Add(payloadByte);
                }
            }
            return fixedPayload.ToArray();
        }

        public static byte[] UnescapeFlagBytes(IEnumerable<byte> bytes)
        {
            var unescapedPayload = new Stack<byte>(bytes.Count());

            foreach (var payloadByte in bytes)
            {
                if (payloadByte == 0x5E)
                {
                    var prev = unescapedPayload.Pop();
                    if (prev == (byte)FramingFlags.CtrlChar)
                    {
                        unescapedPayload.Push((byte)FramingFlags.FrameChar);
                    }
                }
                else if (payloadByte == 0x5D)
                {
                    var prev = unescapedPayload.Peek();
                    if (prev != (byte)FramingFlags.CtrlChar)
                    {
                        unescapedPayload.Push(payloadByte);
                    }
                }
                else
                {
                    unescapedPayload.Push(payloadByte);
                }
            }
            return unescapedPayload.Reverse().ToArray();
        }
    }
}
