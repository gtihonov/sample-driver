﻿using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SubrecordDescription
    {
        public short SubrecordOffset;

        public byte SubrecordType;
    }
}
