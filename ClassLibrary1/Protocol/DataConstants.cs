﻿namespace Driver.S5ComputerInterface.Protocol
{
    public enum DataConstants
    {

        DATA_INVALID_LIMIT = (-32001), /* limit for special invalid data values */
        DATA_INVALID = (-32767), /* there is no valid data */
        DATA_NOT_UPDATED = (-32766), /* data is not updated */
        DATA_DISCONT = (-32765), /* data discontinuity (calibration ...) */
        DATA_UNDER_RANGE = (-32764), /* data exceeds lower valid limit */
        DATA_OVER_RANGE = (-32763), /* data exceeds upper valid limit */
        DATA_NOT_CALIBRATED = (-32762), /* data is not calibrated */

    }
}
