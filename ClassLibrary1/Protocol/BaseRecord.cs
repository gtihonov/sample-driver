using System.Runtime.InteropServices;

namespace Driver.S5ComputerInterface.Protocol
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public abstract class BaseRecord
    {
        public RecordHeader Header;
    }
}