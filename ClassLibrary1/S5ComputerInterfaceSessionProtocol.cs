﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Driver.Base.Layers.Session;
using Driver.Base.Layers.Transport;
using Driver.S5ComputerInterface.Protocol;
using Microsoft.Extensions.Logging;
using Extensions.Utils;

namespace Driver.S5ComputerInterface
{
    public class S5ComputerInterfaceSessionProtocolLayer : BiDirectionalSessionLayer<BaseRecord, DatexRecord>
    {
        private MessageReceivingState _receivingState = MessageReceivingState.Waiting;

        public S5ComputerInterfaceSessionProtocolLayer(ITransport transport,
            ILogger<S5ComputerInterfaceSessionProtocolLayer> logger) : base(transport, logger)
        {
        }


        public override async Task StartAsync()
        {
            await base.StartAsync();

            StartReconnectionCycle();
        }

        public override async Task StopAsync()
        {
            await Transport.DisconnectAsync();
            await base.StopAsync();
        }

        protected override void HandleDataReceived(byte[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                var b = data[i];

                try
                {
                    if (_receivingState == MessageReceivingState.Waiting)
                    {
                        if (IsFrameChar(b) == false)
                        {
                            continue;
                        }

                        _receivingState = MessageReceivingState.ReceiveStarted;
                        Logger.LogTrace("Start of frame was found. State changed to {MessageReceivingState}",
                            _receivingState);
                        Buffer.Add(b);
                        continue;
                    }

                    if (_receivingState == MessageReceivingState.ReceiveStarted)
                    {
                        Buffer.Add(b);

                        if (IsFrameChar(b) == false)
                        {
                            continue;
                        }

                        // Значит мы попали в ситуацию, когда считывание фрейма сообщения началось с середины
                        // В данный момент мы находимся на границе конца первого сообщения и начала следующего
                        // Сбрасываем состояние в ожидание и откатываемся на один байт назад
                        if (Buffer.Count == 2 && Buffer.All(IsFrameChar))
                        {
                            Buffer.Clear();
                            _receivingState = MessageReceivingState.Waiting;
                            --i;
                            continue;
                        }

                        _receivingState = MessageReceivingState.ReceiveCompleted;
                        Logger.LogTrace("End of frame was found. State changed to {MessageReceivingState}",
                            _receivingState);
                    }

                    if (_receivingState == MessageReceivingState.ReceiveCompleted)
                    {
                        var applicationDataBytes = Buffer.Skip(1).Take(Buffer.Count - 2).ToList();

                        var expectedCs = applicationDataBytes.Last();
                        var actualCs =
                            ChecksumUtils.Calculate(applicationDataBytes.Take(applicationDataBytes.Count - 1));

                        if (expectedCs == actualCs)
                        {
                            var unescaped = Framing.EscapeFlagBytes(applicationDataBytes.ToArray());

                            Logger.LogTrace("Received frame. {FrameBytes}",
                                BitConverter.ToString(unescaped));

                            var rc = SerializationUtils.BytesToStruct<DatexRecord>(unescaped);
                            RaiseMessageReceived(rc);
                        }
                        else
                        {
                            Logger.LogError("Failed to validate checksum! {@Info}",
                                new
                                {
                                    ExpectedCs = expectedCs,
                                    ActualCs = actualCs,
                                    BufferLength = Buffer.Count
                                });
                        }

                        Buffer.Clear();
                        _receivingState = MessageReceivingState.Waiting;
                        Logger.LogTrace("Message processed. State changed to {MessageReceivingState}",
                            _receivingState);
                    }
                }
                catch (Exception e)
                {
                    _receivingState = MessageReceivingState.Waiting;
                    Buffer.Clear();

                    Logger.LogError(e, "Exception occured on processing incoming bytes");
                }
            }
        }

        private bool IsFrameChar(byte b)
        {
            return b == (byte) FramingFlags.FrameChar;
        }

        protected override byte[] MakeFrame(BaseRecord msg)
        {
            var rawRecord = SerializationUtils.StructToBytes(msg);

            var applicationData = new List<byte>(rawRecord);

            applicationData.Add(ChecksumUtils.Calculate(rawRecord));

            var fixedApplicationData = Framing.EscapeFlagBytes(applicationData);

            var frame = new List<byte>(fixedApplicationData.Length + 2);

            frame.Add((byte) FramingFlags.FrameChar);
            frame.AddRange(fixedApplicationData);
            frame.Add((byte) FramingFlags.FrameChar);

            return frame.ToArray();
        }

        protected override async void HandleConnectionBroken()
        {
            await Task.Delay(TimeSpan.FromMinutes(5));
            StartReconnectionCycle();
        }

        private async void StartReconnectionCycle()
        {
            if (CancellationTokenSource.Token.IsCancellationRequested == false)
            {
                Logger.LogInformation("Connection broken.Reconnecting...");

                while (true)
                {
                    try
                    {
                        await Transport.ConnectAsync();

                        RaiseSessionEstablished();
                        Logger.LogInformation("Reconnection succeeded");

                        break;
                    }
                    catch (Exception e)
                    {
                        Logger.LogInformation(e, "Failed to reconnect");
                        await Task.Delay(TimeSpan.FromMinutes(5));
                    }
                }
            }
        }
    }
}