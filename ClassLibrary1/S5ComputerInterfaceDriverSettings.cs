﻿using Driver.Base;

namespace Driver.S5ComputerInterface
{
    public class S5ComputerInterfaceDriverSettings : BaseDriverSettings
    {
        public string Version { get; set; }
    }
}