using System;
using System.Collections;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Driver.Base;
using Driver.Contracts;
using Driver.S5ComputerInterface.Protocol;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData;
using Driver.S5ComputerInterface.Protocol.PhysiologicalData.Ext3;
using Driver.S5ComputerInterface.Protocol.WaveformData;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Extensions.Utils;

namespace Driver.S5ComputerInterface
{
    public class S5ComputerInterfaceDriver : BaseDriver<S5ComputerInterfaceSessionProtocolLayer, DatexRecord>
    {
        private readonly TimeSpan _timeoutSpan;

        private readonly short _pollInterval;

        private CancellationTokenSource _timeoutCts = new CancellationTokenSource();

        public S5ComputerInterfaceDriver(string uniqueDeviceId, short pollInterval,
            S5ComputerInterfaceSessionProtocolLayer sessionProtocolLayer,
            ILogger<S5ComputerInterfaceDriver> logger) : base(uniqueDeviceId, sessionProtocolLayer, logger)
        {
            _pollInterval = pollInterval;
            _timeoutSpan = TimeSpan.FromSeconds(pollInterval * 2);
        }

        public override async Task StopAsync()
        {
            await SendPhysioStop();
            await SendWavesStop();
            _timeoutCts.Cancel();

            await base.StopAsync();
        }

        protected override void HandleSessionBroken()
        {
            _timeoutCts.Cancel();
        }

        protected override void HandleSessionEstablished()
        {
            base.HandleSessionEstablished();

            SendPhysioStop();
            SendWavesStop();
            ResetTimeout();
        }

        protected override void SessionProtocolLayerOnMessageReceived(DatexRecord message)
        {
            ResetTimeout();

            var recordDateTime = DateTimeOffset.FromUnixTimeSeconds(message.Header.RelativeTime);
            Logger.LogInformation("Received datex record. RecordNumber: {DatetRecordNumber}, RecordTimestamp: {DatexRecordTimestamp}, RecordDateTime: {DatexRecordDateTime}", message.Header.RecordNumber, message.Header.RelativeTime, recordDateTime);
            switch (message.Header.RecordMainType)
            {
                case RecordMainType.DRI_MT_PHDB:
                {
                    PhysiologicalDataRecord psh = SerializationUtils.BytesToStruct<PhysiologicalDataRecord>(message.Payload.ToArray());
                    ProcessPhysyiologicalRecord(message.Header, psh);
                    break;
                }

                case RecordMainType.DRI_MT_WAVE:
                {
                    ProcessWavesRecord(message.Header, new WaveformDataSubrecords(message.Header.SubrecordDescriptions, message.Payload));
                    break;
                }
            }
        }

        private void ProcessPhysyiologicalRecord(RecordHeader header, PhysiologicalDataRecord psh)
        {
            Logger.LogDebug("Received device message {PsychologicalDataRecord}", psh);

            var paramsMessage = new DeviceParametersEventMessage()
            {
                DeviceId = UniqueDeviceId,
                HappenedAt = DateTime.UtcNow
            };

            for (int i = 0; i < 8; i++)
            {
                SubrecordDescription description = header.SubrecordDescriptions[i];

                if (description.SubrecordType == SubrecorsDecriptionsTest.LastSubrecordType)
                {
                    break;
                }

                PhysiologicalDataSubrecord subrecord = psh.ElementAt(i);

                ushort c = subrecord.ClDrilvlSubt;
                bool[] bInpit = Enumerable.Range(0, 16).Select(x => (c & (0x0001 << x)) != 0).ToArray();

                bool[] typeBits = new[]
                {
                    bInpit[8], bInpit[9], bInpit[10], bInpit[11]
                };

                int rawClass = new BitArray(typeBits).ToNumeral();

                var @class = (PhysiologicalSubrecordClass) rawClass;

                switch (@class)
                {
                    case PhysiologicalSubrecordClass.Basic:
                    {
                        var sub = SerializationUtils.BytesToStruct<PhisiologicalBasicSubrecord>(subrecord.Payload);
                        Logger.LogDebug("Processing subrecord {SubrecordClass} {@Subrecord}; Raw json {SubrecordRaw}", @class, sub,  JsonConvert.SerializeObject(sub));
                        ProcessSubrecord(
                            sub,
                            paramsMessage);
                        break;
                    }
                    case PhysiologicalSubrecordClass.Ext3:
                    {
                        var sub = SerializationUtils.BytesToStruct<PsychologicalExt3Subrecord>(subrecord.Payload);
                        Logger.LogDebug("Processing subrecord {SubrecordClass} {@Subrecord}; Raw json {SubrecordRaw}", @class, sub,  JsonConvert.SerializeObject(sub));
                        ProcessSubrecord(
                            sub,
                            paramsMessage);
                        break;
                    }
                    default: continue;
                }
            }

            if (paramsMessage.Parameters.Count == 0)
            {
                return;
            }

            MessagesSubject.OnNext(paramsMessage);
        }

        private void ProcessWavesRecord(RecordHeader header, WaveformDataSubrecords waveformDataSubrecords)
        {
            var waveRecords = waveformDataSubrecords.ToArray();
            
            foreach ((WaveformType Type, WaveformSubrecord WaveformSubrecord) waveformSubrecordTuple in waveRecords)
            {
                if (waveformSubrecordTuple.Type == WaveformType.DRI_WF_CMD)
                {
                    continue;
                }
                Logger.LogDebug("Processing waves of type {0} {1}", waveformSubrecordTuple.Type.ToString(), JsonConvert.SerializeObject(waveformSubrecordTuple));
                var wavesMessage = new DeviceWaveformData()
                {
                    DeviceId = UniqueDeviceId,
                    ParameterCode = waveformSubrecordTuple.Type.ToString(),
                    HappenedAt = DateTime.UtcNow,
                    RecordNumber = header.RecordNumber,
                    Samples = waveformSubrecordTuple.WaveformSubrecord.Samples.Select(o => (float)o).ToArray()
                };

                WaveformSubject.OnNext(wavesMessage);
            }
        }

        private void ResetTimeout()
        {
            _timeoutCts?.Cancel();

            _timeoutCts = new CancellationTokenSource();

            Task.Delay(_timeoutSpan)
                .ContinueWith((o) =>
                {
                    SendPhysioRequest();
                    SendWavesRequest();
                    ResetTimeout();
                }, _timeoutCts.Token);
        }

        private async Task SendPhysioRequest()
        {
            try
            {
                Logger.LogDebug("Sending physio request");
                var request = new PhysiologicalDataRequest(PhysiologicalSubrecordsTypes.DRI_PH_DISPL, _pollInterval,
                    PhysiologicalDataClasses.DRI_PHDBCL_REQ_BASIC_MASK | PhysiologicalDataClasses.DRI_PHDBCL_REQ_EXT3_MASK);


                var hdr = new RecordHeader()
                {
                    Length = (short) (Marshal.SizeOf<PhysiologicalDataRequest>() + Marshal.SizeOf<RecordHeader>()),
                    RecordMainType = RecordMainType.DRI_MT_PHDB
                };

                hdr.SubrecordDescriptions[0] = new SubrecordDescription()
                {
                    SubrecordType = 0,
                    SubrecordOffset = 0
                };

                hdr.SubrecordDescriptions[1] = new SubrecordDescription()
                {
                    SubrecordType = SubrecorsDecriptionsTest.LastSubrecordType,
                    SubrecordOffset = 0
                };

                var record = new PhysiologicalDataRequestRecord()
                {
                    Header = hdr,
                    PhysiologicalDataRequest = request
                };

                await SessionProtocolLayer.SendMessage(record);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Failed to send physio request");
            }
        }

        private async Task SendWavesRequest()
        {
            try
            {
                Logger.LogDebug("Sending waves request");

                var record = new WaveformDataRequestRecord()
                {
                    Header = new RecordHeader
                    {
                        Length = (short) (Marshal.SizeOf<WaveformRequest>() + Marshal.SizeOf<RecordHeader>()),
                        RecordMainType = RecordMainType.DRI_MT_WAVE,
                        SubrecordDescriptions =
                        {
                            [0] = new SubrecordDescription() {SubrecordType = 0, SubrecordOffset = 0},
                            [1] = new SubrecordDescription()
                            {
                                SubrecordType = SubrecorsDecriptionsTest.LastSubrecordType, SubrecordOffset = 0
                            }
                        }
                    },
                    WaveformRequest = new WaveformRequest(WaveformRequestType.WF_REQ_CONT_START)
                    {
                        WaveformTypes =
                        {
                            [0] = WaveformType.DRI_WF_CO2
                        }
                    }
                };

                await SessionProtocolLayer.SendMessage(record);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Failed to send waves request");
            }
        }

        private async Task SendWavesStop()
        {
            try
            {
                Logger.LogDebug("Sending waves stop request");

                var record = new WaveformDataRequestRecord()
                {
                    Header = new RecordHeader
                    {
                        Length = (short) (Marshal.SizeOf<WaveformRequest>() + Marshal.SizeOf<RecordHeader>()),
                        RecordMainType = RecordMainType.DRI_MT_WAVE,
                        SubrecordDescriptions =
                        {
                            [0] = new SubrecordDescription() {SubrecordType = 0, SubrecordOffset = 0},
                            [1] = new SubrecordDescription()
                            {
                                SubrecordType = SubrecorsDecriptionsTest.LastSubrecordType, SubrecordOffset = 0
                            }
                        }
                    },
                    WaveformRequest = new WaveformRequest(WaveformRequestType.WF_REQ_CONT_STOP)
                };

                await SessionProtocolLayer.SendMessage(record);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Failed to send waves stop request");
            }
        }
        
        private Task SendPhysioStop()
        {
            try
            {
                var request = new PhysiologicalDataRequest(PhysiologicalSubrecordsTypes.DRI_PH_DISPL, 0, 0);

                var hdr = new RecordHeader()
                {
                    Length = (short) (Marshal.SizeOf<PhysiologicalDataRequest>() + Marshal.SizeOf<RecordHeader>()),
                    RecordMainType = RecordMainType.DRI_MT_PHDB,

                };

                hdr.SubrecordDescriptions[0] = new SubrecordDescription()
                {
                    SubrecordType = 0,
                    SubrecordOffset = 0
                };

                hdr.SubrecordDescriptions[1] = new SubrecordDescription()
                {
                    SubrecordType = SubrecorsDecriptionsTest.LastSubrecordType,
                    SubrecordOffset = 0
                };

                var record = new PhysiologicalDataRequestRecord()
                {
                    Header = hdr,
                    PhysiologicalDataRequest = request
                };

                Logger.LogDebug("Sending stop physio request!");

                return SessionProtocolLayer.SendMessage(record);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Failed to send physio stop");
                return Task.CompletedTask;
            }
        }

        private void ProcessSubrecord(PhisiologicalBasicSubrecord subrecord, DeviceParametersEventMessage paramsMessage)
        {
            if (subrecord.NonInvasiveBloodPressure.Sys > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("NonInvSys", new DeviceParameter()
                {
                    Value = subrecord.NonInvasiveBloodPressure.Sys / 100
                });
            }

            if (subrecord.NonInvasiveBloodPressure.Dia > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("NonInvDia", new DeviceParameter()
                {
                    Value = subrecord.NonInvasiveBloodPressure.Dia / 100
                });
            }

            if (subrecord.NonInvasiveBloodPressure.Mean > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("NonInvMean", new DeviceParameter()
                {
                    Value = subrecord.NonInvasiveBloodPressure.Mean / 100
                });
            }

            if (subrecord.Temperature0.Temp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Temperature", new DeviceParameter()
                {
                    Value = subrecord.Temperature0.Temp / 100
                });
            }
            
            if (subrecord.Temperature1.Temp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Temperature1", new DeviceParameter()
                {
                    Value = subrecord.Temperature1.Temp / 100
                });
            }
            
            if (subrecord.Temperature2.Temp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Temperature2", new DeviceParameter()
                {
                    Value = subrecord.Temperature2.Temp / 100
                });
            }
            
            if (subrecord.Temperature3.Temp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Temperature3", new DeviceParameter()
                {
                    Value = subrecord.Temperature3.Temp / 100,
                });
            }
            
            
            if (subrecord.Ecg.HeartRate > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("HR", new DeviceParameter()
                {
                    Value = subrecord.Ecg.HeartRate
                });
            }
            
            if (subrecord.Ecg.ST1 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("ST1", new DeviceParameter()
                {
                    Value = subrecord.Ecg.ST1 / 100
                });
            }
            
            if (subrecord.Ecg.ST2 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("ST2", new DeviceParameter()
                {
                    Value = subrecord.Ecg.ST2 / 100
                });
            }
            
            if (subrecord.Ecg.ST3 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("ST3", new DeviceParameter()
                {
                    Value = subrecord.Ecg.ST3
                });
            }

            if (subrecord.Ecg.RespirationRate > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("RespRate", new DeviceParameter()
                {
                    Value = subrecord.Ecg.RespirationRate
                });
            }
            
            if (subrecord.Spo2.SpO2 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("SpO2", new DeviceParameter()
                {
                    Value = subrecord.Spo2.SpO2 / 100
                });
            }
            
            if (subrecord.Spo2.PulseRate > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("PR", new DeviceParameter()
                {
                    Value = subrecord.Spo2.PulseRate
                });
            }
            
            if (subrecord.Spo2.Modulation > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Modulation", new DeviceParameter()
                {
                    Value = subrecord.Spo2.Modulation
                });
            }
            
            if (subrecord.Spo2.SvO2 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("SpO2_SvO2", new DeviceParameter()
                {
                    Value = subrecord.Spo2.SvO2
                });
            }
            
            if (subrecord.Co2.Et > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("CO2_Et", new DeviceParameter()
                {
                    Value = subrecord.Co2.Et
                });
            }
            
            if (subrecord.Co2.Fi > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("CO2_Fi", new DeviceParameter()
                {
                    Value = subrecord.Co2.Fi
                });
            }
            
            if (subrecord.Co2.Rr > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("CO2_Rr", new DeviceParameter()
                {
                    Value = subrecord.Co2.Rr
                });
            }
            
            if (subrecord.Co2.AmbPress > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("AmbPress", new DeviceParameter()
                {
                    Value = subrecord.Co2.AmbPress
                });
            }
            
            if (subrecord.O2.Et > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("O2_Et", new DeviceParameter()
                {
                    Value = subrecord.O2.Et
                });
            }
            
            if (subrecord.O2.Fi > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("O2_Fi", new DeviceParameter()
                {
                    Value = subrecord.O2.Fi
                });
            }
            
            if (subrecord.N2o.Et > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("N2o_Et", new DeviceParameter()
                {
                    Value = subrecord.N2o.Et
                });
            }
            
            if (subrecord.N2o.Fi > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("N2o_Fi", new DeviceParameter()
                {
                    Value = subrecord.N2o.Fi
                });
            }
            
            if (subrecord.Aa.Et > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("AA_Et", new DeviceParameter()
                {
                    Value = subrecord.Aa.Et
                });
            }
            
            if (subrecord.Aa.Fi > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("AA_Fi", new DeviceParameter()
                {
                    Value = subrecord.Aa.Fi
                });
            }
            
            if (subrecord.Aa.MacSum > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("MacSum", new DeviceParameter()
                {
                    Value = subrecord.Aa.MacSum
                });
            }
            
            if (subrecord.FlowAndVolume.Rr > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowAndVolume_Rr", new DeviceParameter()
                {
                    Value = subrecord.FlowAndVolume.Rr
                });
            }
            
            if (subrecord.FlowAndVolume.Ppeak > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Ppeak", new DeviceParameter()
                {
                    Value = subrecord.FlowAndVolume.Ppeak
                });
            }
            
            if (subrecord.FlowAndVolume.Peep > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Peep", new DeviceParameter()
                {
                    Value = subrecord.FlowAndVolume.Peep
                });
            }

            if (subrecord.FlowAndVolume.Pplat > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Pplat", new DeviceParameter()
                {
                    Value = subrecord.FlowAndVolume.Pplat
                });
            }

            if (subrecord.FlowAndVolume.TvInsp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("TvInsp", new DeviceParameter()
                {
                    Value = subrecord.FlowAndVolume.TvInsp
                });
            }
            
            if (subrecord.FlowAndVolume.TvExp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("TvExp", new DeviceParameter()
                {
                    Value = subrecord.FlowAndVolume.TvExp,
                });
            }
            
            if (subrecord.FlowAndVolume.Compiance > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Compiance", new DeviceParameter()
                {
                    Value = subrecord.FlowAndVolume.Compiance
                });
            }
            
            if (subrecord.FlowAndVolume.MvExp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("MvExp", new DeviceParameter()
                {
                    Value = subrecord.FlowAndVolume.MvExp
                });
            }
            
            if (subrecord.CoWedge.Co > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Co", new DeviceParameter()
                {
                    Value = subrecord.CoWedge.Co
                });
            }
            
            if (subrecord.CoWedge.BloodTemp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("BloodTemp", new DeviceParameter()
                {
                    Value = subrecord.CoWedge.BloodTemp
                });
            }
            
            if (subrecord.CoWedge.Ref > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Ref", new DeviceParameter()
                {
                    Value = subrecord.CoWedge.Ref
                });
            }
            
            if (subrecord.CoWedge.Pcwp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Pcwp", new DeviceParameter()
                {
                    Value = subrecord.CoWedge.Pcwp
                });
            }
            
            if (subrecord.Nmt.T1 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("T1", new DeviceParameter()
                {
                    Value = subrecord.Nmt.T1,
                });
            }

            if (subrecord.Nmt.Tratio > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Tratio", new DeviceParameter()
                {
                    Value = subrecord.Nmt.Tratio,
                });
            }
            
            if (subrecord.Nmt.Ptc > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("Ptc", new DeviceParameter()
                {
                    Value = subrecord.Nmt.Ptc
                });
            }
            
            if (subrecord.EcgExtra.Hr_Ecg > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("EcgExtra_Hr_Ecg", new DeviceParameter()
                {
                    Value = subrecord.EcgExtra.Hr_Ecg
                });
            }
            
            if (subrecord.EcgExtra.Hr_Max > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("EcgExtra_Hr_Max", new DeviceParameter()
                {
                    Value = subrecord.EcgExtra.Hr_Max
                });
            }
            
            if (subrecord.EcgExtra.Hr_Min > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("EcgExtra_Hr_Min", new DeviceParameter()
                {
                    Value = subrecord.EcgExtra.Hr_Min,
                });
            }
            
            if (subrecord.Svo2.SVo2 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("SvO2_SvO2", new DeviceParameter()
                {
                    Value = subrecord.Svo2.SVo2
                });
            }
        }

        private void ProcessSubrecord(PsychologicalExt3Subrecord subrecord, DeviceParametersEventMessage paramsMessage)
        {
            if (subrecord.Gasex.Vo2 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("GasexVo2", new DeviceParameter()
                {
                    Value = subrecord.Gasex.Vo2 / 100
                });
            }

            if (subrecord.Gasex.VCo2 > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("GasexVCo2", new DeviceParameter()
                {
                    Value = subrecord.Gasex.VCo2 / 100
                });
            }

            if (subrecord.Gasex.Ee > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("GasexEe", new DeviceParameter()
                {
                    Value = subrecord.Gasex.Ee / 100
                });
            }

            if (subrecord.Gasex.Rq > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("GasexRq", new DeviceParameter()
                {
                    Value = subrecord.Gasex.Rq / 100,
                });
            }

            if (subrecord.FlowVol2.Ipeep > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2Ipeep", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.Ipeep / 100
                });
            }

            if (subrecord.FlowVol2.Pmean > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2Pmean", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.Pmean / 100
                });
            }

            if (subrecord.FlowVol2.Raw > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2Raw", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.Raw / 100
                });
            }

            if (subrecord.FlowVol2.MvInsp > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2MvInsp", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.MvInsp / 100
                });
            }

            if (subrecord.FlowVol2.Epeep > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2Epeep", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.Epeep / 100
                });
            }

            if (subrecord.FlowVol2.MvSpont > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2MvSpont", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.MvSpont / 100,
                });
            }

            if (subrecord.FlowVol2.IeRatio > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2IeRatio", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.IeRatio / 100 
                });
            }

            if (subrecord.FlowVol2.InspTime > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2InspTime", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.InspTime / 100
                });
            }

            if (subrecord.FlowVol2.ExpTime > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2ExpTime", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.ExpTime / 100
                });
            }

            if (subrecord.FlowVol2.StaticCompliance > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2StaticCompliance", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.StaticCompliance / 100,
                });
            }

            if (subrecord.FlowVol2.StaticPplat > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2StaticPplat", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.StaticPplat / 100
                });
            }

            if (subrecord.FlowVol2.StaticPeepe > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2StaticPeepe", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.StaticPeepe / 100
                });
            }

            if (subrecord.FlowVol2.StaticPeepi > Constants.MaxSystemValue)
            {
                paramsMessage.Parameters.Add("FlowVolGroup2StaticPeepi", new DeviceParameter()
                {
                    Value = subrecord.FlowVol2.StaticPeepi / 100
                });
            }
        }
    }
}