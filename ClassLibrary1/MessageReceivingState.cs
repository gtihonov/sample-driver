﻿namespace Driver.S5ComputerInterface
{
    public enum MessageReceivingState
    {
        Waiting,
        ReceiveStarted,
        ReceiveCompleted
    }
}